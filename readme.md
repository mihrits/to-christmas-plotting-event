# TO Cosmologists plotting event 2019 Christmas

The aim of the event is to produce some nice-looking plots, create Christmas atmosphere, and maybe settle some personal quarrels. Everybody are welcome to join and submit their plots.

## How to participate

Submit your work of art (plot in pdf and code) before 14:00 on 20.12.19 to moorits.mihkel.muru@ut.ee. Submitted plots must follow rules listed below.

## Rules

1. Plots must use the given input data. Data may be reformated, but not modified. See Data section for more info about the data.
2. General layout for the plot is shown on the figure below. The submitted plots must follow these guidelines, but may have some smaller tweaks and modifications.
3. If something is unspecified (e.g. colors, sizes, ratios, orientations), then it is left for the author to be designed to be visually pleasing.

### Layout

![alt text](https://gitlab.com/mihrits/to-christmas-plotting-event/raw/master/layout.jpg "Layout scheme")

1. Upper left panel is a scatter plot with data from `data[1-4].dat`.
2. Lower panel is a 3D bar plot with data from `data5.dat`.
3. On the right side of the lower panel, there is a figure inside the larger figure, i.e. "floating figure". That figure is a heatmap with data from `data8.dat`.
4. Upper right area is left for an image of the author (may be hand-drawn, or modified picture, or whichever way the author desires to present him-/herself), a scientific holiday wish, and author's name.
5. Middle right panel contains two plots. Upper one is a polar plot with data from `data6.dat`. Lower one is a multi-layered pie plot with data from `data7.dat`, this plot has to be labeled, has to have a legend, or a mix of those two. There has to be a large T on left side of polar plot, to spell out "TO".

### Data
* `data[1-4].dat` has 3 columns, x and y coordinates, and relative size of the point on the scatter plot.
* `data5.dat` has 3 columns, x and y coordinates, and height of the bar.
* `data6.dat` has 2 columns, angle in radians and radius, and 3 different set on points seperated with 2 linebreaks.
* `data7.dat` has 3 columns representing 3 different properties and 4 rows representing 4 different layers. File has names of the properties and the layers as comments starting with `#` symbol.
* `data8.dat` has 25 rows and 16 columns, each containing the value for the heatmap for that coordinate.

## Winner

Winner of the event will be chosen based on visual clarity and beauty of the plot. The structure and clarity of the code might be used as a tie-breaker. Of course, this all will be subjective. Decision will be made at the Cookies seminar on Friday with seminar participants.
