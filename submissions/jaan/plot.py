import numpy as np
import astropy.io.ascii as at
import matplotlib.pyplot as plt
from scipy.stats import kde
import matplotlib.patheffects as pe



data1 = at.read('data/data1.dat')
data2 = at.read('data/data2.dat')
data3 = at.read('data/data3.dat')
data4 = at.read('data/data4.dat')
data5 = at.read('data/data5.dat')
data6 = at.read('data/data6.dat')
data7 = at.read('data/data7.dat')
data8 = at.read('data/data8.dat')
from matplotlib import cbook



plt.figure(1,figsize=(12, 15), dpi=100, facecolor='w', edgecolor='k')



imageFile = cbook.get_sample_data("/wrk/jaanlaur/topz/plotplot/ugly-sweater-transparent-background-png-christmas-2183dd0231ecdfbf.png")
image = plt.imread(imageFile)
plt.imshow(image,alpha=0.2)
plt.axis('off')



ax_full = plt.axes([0,0,1,1])


#import  matplotlib.font_manager
#flist = matplotlib.font_manager.get_fontconfig_fonts()
#names = [matplotlib.font_manager.FontProperties(fname=fname).get_name() for fname in flist]
#print(names)

#quit()


font1='monospace'
#font1='OpenSans-Semibold'
#font1='Caladea-Regular'
#font1='madan'

#plt.rcParams['font.family'] = 'serif'


plt.text(0.865, 0.93,'Good... Good...',   horizontalalignment='center',   verticalalignment='center',   transform = ax_full.transAxes,fontsize=20,fontname=font1)
plt.text(0.855, 0.88,'Let the Science',   horizontalalignment='center',   verticalalignment='center',   transform = ax_full.transAxes,fontsize=18,fontname=font1)
plt.text(0.855, 0.84,'flow through you!',   horizontalalignment='center',   verticalalignment='center',   transform = ax_full.transAxes,fontsize=18,fontname=font1)

plt.text(0.855, 0.76,'Jaan',   horizontalalignment='center',   verticalalignment='center',   transform = ax_full.transAxes,fontsize=40,family='fantasy')

ax_full.patch.set_alpha(0)










#definitions for the axes
left, width = 0.02, 0.57
bottom, height = 0.4, 0.55



rect_scatter = [left, bottom, width, height]

ax_scatter = plt.axes(rect_scatter)

ax_scatter.patch.set_alpha(0.88)






alpha_black=0.6
step=0.035
dot=33




ax_scatter.scatter(data1['col1']+step,data1['col2']-step,s=data1['col3']*dot, color='black',alpha=alpha_black)
ax_scatter.scatter(data1['col1'],data1['col2'],s=data1['col3']*20, color='black')
ax_scatter.scatter(data1['col1'],data1['col2'],s=data1['col3']*18, color='forestgreen')
ax_scatter.scatter(data2['col1']+step,data2['col2']-step,s=data2['col3']*dot, color='black',alpha=alpha_black)
ax_scatter.scatter(data2['col1'],data2['col2'],s=data2['col3']*20, color='black')
ax_scatter.scatter(data2['col1'],data2['col2'],s=data2['col3']*18, color='gold')
ax_scatter.scatter(data3['col1']+step,data3['col2']-step,s=data3['col3']*dot, color='black',alpha=alpha_black)
ax_scatter.scatter(data3['col1'],data3['col2'],s=data3['col3']*20, color='black')
ax_scatter.scatter(data3['col1'],data3['col2'],s=data3['col3']*18, color='coral')
ax_scatter.scatter(data4['col1']+step,data4['col2']-step,s=data4['col3']*dot, color='black',alpha=alpha_black)
ax_scatter.scatter(data4['col1'],data4['col2'],s=data4['col3']*20, color='black')
ax_scatter.scatter(data4['col1'],data4['col2'],s=data4['col3']*18, color='lightblue')

ax_scatter.set_yticklabels([])
ax_scatter.set_xticklabels([])
ax_scatter.tick_params(axis='both', which='both', length=0)








#definitions for the axes
left, width = 0.6, 0.15
bottom, height = 0.73, 0.22



rect_scatter = [left, bottom, width, height]

ax_face = plt.axes(rect_scatter)



imageFile = cbook.get_sample_data("/wrk/jaanlaur/topz/plotplot/80207045_2444342525827844_1831705617899716608_n.jpg")
image = plt.imread(imageFile)
plt.imshow(image)
plt.axis('off')






left, width = 0.62, 0.35
bottom, height = 0.4, 0.31



ax_mid_panel = plt.axes([left, bottom, width, height])
ax_mid_panel.patch.set_alpha(0.88)
ax_mid_panel.set_yticklabels([])
ax_mid_panel.set_xticklabels([])
ax_mid_panel.tick_params(axis='both', which='both', length=0)










left, width = 0.8, 0.13
bottom, height = 0.55, 0.13


ax_polar = plt.axes([left, bottom, width, height],projection='polar')


#ax = plt.subplot(111, projection='polar')

ax_polar.scatter(data6['col1'], data6['col2'],color='black',s=13)
ax_polar.scatter(data6['col1'], data6['col2'],color='royalblue',s=10)

ax_polar.set_yticklabels([])
ax_polar.set_xticklabels([])
ax_polar.tick_params(axis='both', which='both', length=0)




plt.text(0.73, 0.595,'T',   horizontalalignment='center',   verticalalignment='center',   transform = ax_full.transAxes,fontsize=130,family='fantasy',color='black',alpha=0.7)
plt.text(0.72, 0.605,'T',   horizontalalignment='center',   verticalalignment='center',   transform = ax_full.transAxes,fontsize=130,family='fantasy',color='royalblue')















left, width = 0.02, 0.95
bottom, height = 0.05, 0.33



ax_low_panel = plt.axes([left, bottom, width, height])
ax_low_panel.patch.set_alpha(0.88)
ax_low_panel.set_yticklabels([])
ax_low_panel.set_xticklabels([])
ax_low_panel.tick_params(axis='both', which='both', length=0)





#filename='plotplotplot.pdf'
#plt.savefig(filename, bbox_inches = 'tight',pad_inches = 0.05)
#print("figure saved: "+ filename)




plt.show()