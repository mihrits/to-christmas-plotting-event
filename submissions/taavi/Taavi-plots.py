#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D 


# In[2]:


d = []

for i in np.arange(1, 9):
    d.append(np.loadtxt('data{:d}.dat'.format(i)))


# In[3]:


fig = plt.figure(figsize=(8,8))
ax1 = plt.axes([0.01, 0.46, 0.6, 0.53])
ax1.tick_params(axis='both', bottom=False, labelbottom=False,
                left=False, labelleft=False)
colors = ['black', 'red', '#888888', '#aaaaaa']
styles = ['o', 'o', 'o', 'x']

for i in np.arange(4):
    ax1.scatter(d[i][:,0], d[i][:,1], marker=styles[i],
                s=d[i][:,2]*7, c=colors[i])

# Box around author box
ax_author = plt.axes([0.62, 0.85, 0.37, 0.14])
ax_author.tick_params(axis='both', bottom=False, labelbottom=False,
                     left=False, labelleft=False)
ax_author.text(0.5, 0.5, 'Taavi', fontsize=36, color='#0088aa',
               horizontalalignment='center', verticalalignment='center',
               transform=ax_author.transAxes)
    
# Box around polar plot and pie chart
ax_right = plt.axes([0.62, 0.46, 0.37, 0.38])
ax_right.tick_params(axis='both', bottom=False, labelbottom=False,
                     left=False, labelleft=False)
ax_right.text(0.45, 0.68, 'T', fontsize=132, horizontalalignment='right',
              verticalalignment='center', transform=ax_right.transAxes)

# Polar plot
ax3 = plt.axes([0.79, 0.65, 0.18, 0.18], projection='polar')
ax3.tick_params(axis='both', bottom=False, labelbottom=False,
                left=False, labelleft=False)
ax3.grid(False)
ax3.plot(d[5][:,0], d[5][:,1], 'o', markersize=2.0)

# Pie chart
ax4 = plt.axes([0.77, 0.45, 0.22, 0.22])

for i in np.arange(4):
    r = 1. - i * 0.25
    ax4.pie(d[6][3-i,:], radius=r)

# Box around 3D bar plot and heatmap
ax_bottom = plt.axes([0.01, 0.01, 0.98, 0.44])
ax_bottom.tick_params(axis='both', bottom=False, labelbottom=False,
                      left=False, labelleft=False)

# 3D bar plot
ax2 = plt.axes([0.02, 0.02, 0.58, 0.42], projection='3d')
ax2.tick_params(axis='both', bottom=False, labelbottom=False,
                left=False, labelleft=False)
top = d[4][:,2]
bottom = np.zeros_like(top)
width = 0.8
depth = 0.4
ax2.bar3d(d[4][:,0], d[4][:,1], bottom, width, depth, top, shade=True)

# Heatmap
ax5 = plt.axes([0.65, 0.05, 0.3, 0.35])
ax5.tick_params(axis='both', bottom=False, labelbottom=False,
                left=False, labelleft=False)
ax5.imshow(d[7], cmap='Blues')

# Save figure
fig.set_dpi(300)
fig.savefig('Taavi-plots.pdf', format='pdf')


# In[ ]:




